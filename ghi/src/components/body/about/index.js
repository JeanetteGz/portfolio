import React from 'react';
import './about.css';
import SocialContact from '../../common/social-contact';

function About() {
  return (
    <div className='about'>
      <div className='about-top'>
          <div className='about-info'>
          Hello 👋🏻, I am 
          <br /> <span className='info-name'>Jeanette Gonzalez</span>
          <br /> and I am a Software Engineer!👩🏻‍💻
          </div>
          <div className='about-photo'>
            <img src={require('../../../assests/coding.png')} alt = '' className='picture' />
          </div>
        </div>
      <SocialContact />
    </div>
  );
}

export default About;
