export const SocialData = [
  {
    platform: "LinkedIn",
    link: "https://www.linkedin.com/in/jeanetteglz/",
    icon: require("../../assests/icons/linkedin.png")
  },
  {
    platform: "GitLab",
    link: "https://gitlab.com/JeanetteGz",
    icon: require("../../assests/icons/gitlab.png")
  },
];
