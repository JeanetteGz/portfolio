import React from 'react';
import './mobile.css';

function Mobile({ isOpen, setIsOpen }) {
  return (
    <div className='mobile'>
      <div className='close-icon' onClick={() => setIsOpen(!isOpen)}>
        <i class="fi fi-rs-circle-xmark"></i>
      </div>
      <div className='mobile-options'>
        <div className='mobile-option'>
          <a href='#projects'>
            <i className="fi fi-rr-edit-alt option-icon"></i>My Projects
          </a>
        </div>
        <div className='mobile-option'>
          <a href='#skills'>
            <i className="fi fi-rr-display-code"></i>My Skills
          </a>
        </div>
        <div className='mobile-option'>
          <a href='#work'>
            <i className="fi fi-rr-briefcase option-icon"></i>Work Experience
          </a>
        </div>
        <div className='mobile-option'>
          <a href='#contact'>
            <i className="fi fi-rr-circle-user option-icon"></i>Contact Information
          </a>
        </div>
      </div>
    </div>
  );
}

export default Mobile;
