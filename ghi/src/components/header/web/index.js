import React from 'react';
import './web.css';

function Web() {
    return <div className="web">
        <div className='web-option'>
            <a href='#projects'>
            <i class="fi fi-rr-edit-alt option-icon"></i>My Projects
            </a>
        </div>
        <div className='web-option'>
            <a href='#skills'>
            <i class="fi fi-rr-display-code"></i>My Skills
            </a>
        </div>
        <div className='web-option'>
            <a href='#work'>
            <i class="fi fi-rr-briefcase option-icon"></i>Work Experience
            </a>
        </div>
        <div className='web-option'>
            <a href='#contact'>
            <i class="fi fi-rr-circle-user option-icon"></i>Contact Information
            </a>
        </div>
    </div>;
}

export default Web;
